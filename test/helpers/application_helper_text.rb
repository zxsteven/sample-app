require 'test_helper'

class ApplicationHelper < ActionView::TestCase
  
  test "full title helper" do
    assert_equal full_title, FILL_IN
    assert_equal full_title('help'), FILL_IN
  end
end